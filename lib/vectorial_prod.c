#include "caos.h"
#include "vectorial_prod.h"
#include "allocate.h"

void cargar_vector(double *A, int count) {
  double numero;
  for (size_t i = 0; i < count; i++) {
          numero = ((double)(caos(1000000)) + 100000) / 1000000;
          A[i] = caos(50) + numero;
  }
}

void mostrar_vector(double *u, int N) {
  printf("(%f", u[0]);
  for (size_t i = 1; i < N; i++) {
    printf(", %f", u[i]);
  }
  printf(")\n\n");
}

double *producto_vectorial(double *A, double *B, int N) {

  double *C = crear_vector(N);

  C[0] = A[1] * B[2] - (B[1] * A[2]);
  C[1] = -(A[0] * B[2] - B[0] * A[2]);
  C[2] = A[0] * B[1] - (B[0] * A[1]);

  printf("-> [%f, %f, %f]x[%f, %f, %f] = [%f, %f, %f] \n", A[0], A[1],A[2], B[0], B[1], B[2], C[0], C[1], C[2]);
  return C;
}

void producto_escalar(double *vector, double escalar, int size) {
  for (size_t i = 0; i < size; i++) {
    vector[i] = vector[i] * escalar;
  }
}

void cargar_matriz(double **matriz, int M, int N) {
  double numero;
  for (size_t j = 0; j < M; j++) {
    for (size_t i = 0; i < N; i++) {
            numero = ((double)(caos(1000000)) + 100000) / 1000000;
            matriz[j][i] = caos(50) + numero;
    }
  }

}

void mostrar_matriz(double **Matriz, int M, int N) {
        for (size_t i = 0; i < M; i++) {
                for (size_t j = 0; j < N; j++) {
                        printf("%f\t", Matriz[i][j]);
                }
                printf("\n");
        }
        printf("\n\n");
}

double **producto_matricial(double **A, double **B, int M, int N, int P) {
  double **C = crear_matriz(M, P);
  int sum = 0;
  for (size_t i = 0; i < M; i++) {
    for (size_t j = 0; j < P; j++) {
      for (size_t k = 0; k < N; k++) {
        sum += A[i][k] * B[k][j];
      }
      C[i][j] = sum;
      sum = 0;
    }
  }
  return C;
  // printf("Producto de las matrices A(%d,%d)*B(%d,%d):\n",M,N,N,P);
  // mostrar_matriz(C, M, P);
}

void escalar_matriz(double **Matriz, int M, int N, double escalar) {
        for (size_t i = 0; i < M; i++) {
                for (size_t j = 0; j < N; j++) {
                      Matriz[i][j] = Matriz[i][j] * escalar;
                }
        }
}
void transponer_matriz(double ***matriz_ptr, int M, int N) {
  double **temporal = crear_matriz(N, M);
  double **matriz = *matriz_ptr;

  // Calcular la transpuesta
  for (size_t i = 0; i < M; i++) {
          for (size_t j = 0; j < N; j++) {
              temporal[j][i] = matriz[i][j];
          }
  }
  // mostrar_matriz(temporal, N, M);

  // Redimensionar el arreglo
  matriz = realloc(matriz, N * sizeof(double *));
  for(size_t i = 0; i < N; i++) {
	  if(i < M) {
		  matriz[i] = realloc(matriz[i], M * sizeof(double));
	  }
	  else {  // Si i >= M, es una fila "nueva", no estaba antes, por lo que realloc no sirve, hay que usar malloc
		  matriz[i] = malloc(M * sizeof(double));
	  }
  }

  *matriz_ptr = matriz; // atualizamos el valor del puntero para que se vea desde afuera

  // Copiar la transpuesta
  for (size_t i = 0; i < N; i++) {
    for (size_t j = 0; j < M; j++) {
      matriz[i][j] = temporal[i][j];
    }
  }
  borrar_matriz(temporal, M);
}
