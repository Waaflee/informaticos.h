#include "allocate.h"

double *crear_vector(int N) {
        double *vector = malloc(N * sizeof(double));
        memset(vector, 0, N);
        return vector;
}

void borrar_vector(double *vector) {
        free(vector);
}

double **crear_matriz(int M, int N) {

        double **matriz = malloc(M * sizeof(double *));
        memset(matriz, 0, M);
        for(size_t i = 0; i < M; i++) {
                matriz[i] = malloc(N * sizeof(double));
                memset(matriz[i], 0, N);
        }
        return matriz;
}

void borrar_matriz(double **matriz, int N) {
        for(size_t i = 0; i < N; i++) {
                free(matriz[i]);
        }
        free(matriz);
}
