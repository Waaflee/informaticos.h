#ifndef ALLOCATE
#define ALLOCATE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

double *crear_vector(int N);
void borrar_vector(double *vector);

double **crear_matriz(int M, int N);
void borrar_matriz(double **vector, int N);

#endif
