#ifndef VECTORIAL_PROD
#define VECTORIAL_PROD

#include "caos.h"
#include "allocate.h"

// typedef struct Vector {
//   double x;
//   double y;
//   double z;
// } vector;

double *producto_vectorial(double *A, double *B, int N);
void producto_escalar(double *vector, double escalar, int size);
void cargar_vector(double *A, int count);
void mostrar_vector(double *u, int N);

void mostrar_matriz(double **Matriz, int M, int N);
void cargar_matriz(double **matriz, int M, int N);
double **producto_matricial(double **A, double **B, int M, int N, int P);
void escalar_matriz(double **Matriz, int M, int N, double escalar);
void transponer_matriz(double ***matriz, int M, int N);


#endif
