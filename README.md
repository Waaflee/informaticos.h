# **Informaticos.h**
### Prácticos del Cursado de Informática - UNC 2017
## Dependencias
 - gcc
 - [meson](http://mesonbuild.com/index.html) para compilar
 - python 3

## Modo de uso
En el práctico 3 se puede compilar tracicionalmente con gcc `gcc programa.c` salvo el ejercicio 17 donde debe enlazarce junto con `caos.c` presente en la carpeta `lib` y otras contdas excepciones donde se encuentra incluida la libreria de matemática, en cuyo caso debe indicarsele al compilador que debe enlazarla también `gcc -lm ejercicio.c`

En el práctico 4 dado que hay una creciente complejización en gcc para compilar y enlazar los archivos a medida que aumenta su cantidad se ha optado por incluir un `meson.build` el cual contiene las instrucciones para compilar cada paquete en un directorio especialmente dedicado para tal fin. Para utilizarlo debe crear un directiorio de compilación con `meson builddir` y luego `cd builddir && ninja`.

En el Práctico 5 el ejercicio 5 requiere una lista de las habitaciones presentes en una casa junto con sus propiedades (largo, alto, etc), provista por el modulo `casa.py` que puede ser generado y regenerado con `casagenerator.py` para probar con distintas configuraciones de casas.
