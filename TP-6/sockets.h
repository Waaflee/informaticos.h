#ifndef SOCKETS
#define SOCKETS

#include <semaphore.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

void error(char *msg);
void serve(int puerto);
void client(char *hostname, int puerto);

#endif
