// El proceso principal (Proceso A), crea 2 procesos Bs (Procesos B y C)
// gcc -lpthread e1.c -o e1

#include "files.h"
#include "sockets.h"
#include "threads.h"
// #include <bits/signum.h>
#include <signal.h>
#include <string.h>
#include <time.h>

void set_seed();
int caos(int MAXIMO);

int join = 0;
// int indice = 0;
char buffer_compartido[128];
int sizebfr = 0;
int puerto;

int thread_3 = 0;
int thread_4 = 0;

pthread_mutex_t mutex;
pthread_barrier_t barr;

void handler(int holi);

int main(int argc, char const *argv[]) {
  set_seed();
  pthread_mutex_init(&mutex, NULL);
  pthread_barrier_init(&barr, NULL, 2);
  puerto = caos(50000) + 3000; // por si falló al correr y el puerto queda
  // sin cerrar
  pid_t pid_B;
  pid_B = fork();
  if (pid_B == -1) {
    error("Error al crear proceso B");
  } else if (pid_B == 0) {
    printf("Soy el  proceso B!\n");
    serve(puerto);
    signal(SIGHUP, handler);
  } else {
    printf("Soy el proceso A!\n");
    pid_t pid_C;
    pid_C = fork();
    if (pid_C == -1) {
      error("Error al crear proceso C");
    } else if (pid_C == 0) {
      printf("Soy el  proceso C!\n");
      getchar();
      kill(pid_B, SIGHUP);

    } else {
      client("localhost", puerto);
      exit(0);
    }
  }
  return 0;
}

void *trhead_h2(void *arg) {
  printf("Thread H2 escuchando...\n");
  int n, newsockfd = *((int *)arg);
  int rc;
  char header[128];
  while (join != 1) {
    rc = pthread_barrier_wait(&barr);
    if (thread_3 == 0 && thread_4 == 0) {
      pthread_mutex_lock(&mutex);
      bzero(header, 128);
      n = read(newsockfd, header, 128);
      if (n < 0) {
        error("ERROR leyendo el socket (Server)");
      }
      strncpy(buffer_compartido, header, strlen(header));
      // if (strlen(header)) {
      // sizebfr = strlen(header);
      printf("header -> %s, SIZE %d\n", header, strlen(header));
      char ok[] = "H2 -> Todo Ok\n";
      n = write(newsockfd, ok, strlen(ok));
      if (n < 0) {
        error("ERROR writing to socket (Server)");
      }
      if (strlen(header) > 0) {
        sizebfr++;
      }
      thread_3 = 1;
      pthread_mutex_unlock(&mutex);
      usleep(5000);
    }
  }
  // join = 1;
  close(newsockfd);
  return NULL;
}

void *trhead_h3(void *arg) {
  char localbfr[128];
  int version = 0;
  int rc;
  while (join != 1) {
    rc = pthread_barrier_wait(&barr);
    if (thread_3 == 1 && thread_4 == 0 && sizebfr > version) {
      pthread_mutex_lock(&mutex);
      bzero(localbfr, 128);
      strcpy(localbfr, buffer_compartido);
      printf("H3 -> %s\n", localbfr);
      thread_4 = 1;
      version = sizebfr;
      bzero(localbfr, 128);
      pthread_mutex_unlock(&mutex);
    }
  }
  return NULL;
}

void *trhead_h4(void *arg) {
  FILE *fp;
  int rc;
  char localbfr[128];
  int version = 0;
  // char localbfr2[128];
  fp = fopen("recibido.txt", "w");
  while (join != 1) {
    rc = pthread_barrier_wait(&barr);
    if (thread_3 == 1 && thread_4 == 1 && sizebfr > version) {
      bzero(localbfr, 128);
      pthread_mutex_lock(&mutex);
      // if (localbfr == localbfr2) {
      //         continue;
      // }
      strcpy(localbfr, buffer_compartido);
      fwrite(localbfr, sizeof(char), 10, fp);
      fflush(fp);
      // strcpy(localbfr2, buffer_compartido);
      thread_3 = 0;
      thread_4 = 0;
      version = sizebfr;
      pthread_mutex_unlock(&mutex);
    }
  }
  fclose(fp);
  return NULL;
}

void error(char *msg) {
  perror(msg);
  exit(1);
}

void serve(int puerto) {
  int sockfd, newsockfd, clilen, n;
  struct sockaddr_in serv_addr, cli_addr;
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    error("ERROR al abrir socket");
  }
  bzero((char *)&serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(puerto);
  serv_addr.sin_addr.s_addr = INADDR_ANY; // igual a 127.0.0.1

  if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    error("ERROR on binding");
  }
  listen(sockfd, 5);

  clilen = sizeof(cli_addr);

  newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen); // blocked

  pthread_t t2;
  pthread_t t3;
  pthread_t t4;

  pthread_create(&t2, NULL, trhead_h2, &newsockfd);
  pthread_create(&t3, NULL, trhead_h3, NULL);
  pthread_create(&t4, NULL, trhead_h4, NULL);

  pthread_join(t2, NULL);
  printf("newsockfd closed\n");
  pthread_join(t3, NULL);
  pthread_join(t4, NULL);
  printf("All threads joined\n");
  close(sockfd);
  printf("sockfd closed\n");
  pthread_mutex_destroy(&mutex);
}

void client(char *hostname, int puerto) {
  int sockfd, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    error("ERROR opening socket");
  }
  server = gethostbyname(hostname);
  if (server == NULL) {
    fprintf(stderr, "ERROR, no such host");
    exit(0);
  }
  bzero((char *)&serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,
        server->h_length);
  serv_addr.sin_port = htons(puerto);

  if (connect(sockfd, &serv_addr, sizeof(serv_addr)) < 0) {
    error("ERROR connecting");
  } else {
    printf("Conectado al Server!\n");
  }
  long largo;
  char mensaje[128];
  char archivo[] = "noseculpeanadie.txt";
  FILE *fp = fopen(archivo, "r");
  fseek(fp, 0, SEEK_END);
  largo = ftell(fp);
  char *buffer = calloc(largo, sizeof(char));
  fseek(fp, 0, SEEK_SET);
  for (size_t i = 0; i < largo / 10 + 1; i++) {
    fread(buffer, sizeof(char), 10, fp);
    strncpy(mensaje, buffer, 128);
    n = write(sockfd, mensaje, strlen(mensaje));
    n = read(sockfd, mensaje, strlen(mensaje) * 2);
  }
  join = 1;
  printf("%s\n", mensaje);
  fclose(fp);
}

void set_seed() {
  time_t segundosEpoch = time(NULL);
  srand(segundosEpoch);
}
int i;
int caos(int MAXIMO) {
  i = 1;
  return i * (rand() % MAXIMO);
}

void handler(int holi) {
  printf("INTERRUPCIÓN\n");
  if (holi == SIGHUP) {
    join = 1;
  }
}
