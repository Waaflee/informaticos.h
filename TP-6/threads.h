#ifndef THREADS
#define THREADS

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>

// void *trhead_h1(void *arg);
void *trhead_h2(void *arg);
void *trhead_h3(void *arg);
void *trhead_h4(void *arg);


#endif
