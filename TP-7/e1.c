#include <math.h>
#include <stdio.h>

void mostrar_matriz();
void exlplorarNodo(int M, int N);
int menor(int *M, int *N);

int matrix[10][10] = {
    {1, 0, 0, 0, 0, 1, 1, 1, 1, 1}, {1, 1, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 1, 0, 0, 0, 0}, {0, 1, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 1, 0, 0, 0, 0}, {0, 1, 0, 0, 0, 1, 1, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 1, 1, 1, 1}, {0, 1, 0, 0, 0, 0, 0, 0, 0, 1},
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 1}, {0, 1, 1, 1, 1, 1, 1, 1, 1, 0}};
int A[2] = {0, 1};
int B[2] = {9, 9};

int EXITO = 0;

int main(int argc, char const *argv[]) {
  int dale = 0;
  while (!dale) {
    printf("Ingrese Coordenadas iniciales 'x,y' \n");
    scanf("%d,%d", &A[0], &A[1]);
    printf("Ingrese Coordenadas finales 'x,y' \n");
    scanf("%d,%d", &B[0], &B[1]);
    if (A[0] < 10 && A[1] < 10 && B[0] < 10 && B[1] < 10) {
      dale = 1;
    } else {
      printf("Ingresá números enteros positivos menores a 10 \n");
    }
  }
  printf("A = %d,%d\n", A[0], A[1]);
  printf("B = %d,%d\n", B[0], B[1]);

  exlplorarNodo(A[0], A[1]);
  if (EXITO) {
    mostrar_matriz();
  } else {
    printf("No se encontró un camino \t :(\n");
  }
  return 0;
}

void mostrar_matriz() {
  printf("\n");
  for (size_t i = 0; i < 10; i++) {
    for (size_t j = 0; j < 10; j++) {
      printf("%d\t", matrix[i][j]);
    }
    printf("\n");
  }
  printf("\n\n");
}

void exlplorarNodo(int M, int N) {
  if (!EXITO) {
    printf("-> Explorando nodo [%d][%d]\n-> Valor encontrado = %d\n", M, N,
           matrix[M][N]);
    if ((M >= 0 && M < 10) && ((N >= 0 && N < 10))) {
      if (matrix[M][N] != 0 && matrix[M][N] != 1) {
        printf("-> Nodo ya visitado! \n");
      }
    }
    if (matrix[M][N] == 0) {
      if (M == B[0] && N == B[1]) {
        printf("Misión Cumplida!\n");
        EXITO = 1;
      }
      matrix[M][N] = 888;
      if (M > 0 && N > 0) {
        for (size_t i = M - 1; i < M + 2; i++) {
          for (size_t j = N - 1; j < N + 2; j++) {
            int actual[2] = {M, N};
            int posible[2] = {i, j};
            if (menor(actual, posible)) {
              exlplorarNodo(i, j);
            }
          }
        }
      } else if (M == 0 && N > 0) {
        for (size_t i = M; i < M + 2; i++) {
          for (size_t j = N - 1; j < N + 2; j++) {
            int actual[2] = {M, N};
            int posible[2] = {i, j};
            if (menor(actual, posible)) {
              exlplorarNodo(i, j);
            }
          }
        }
      } else if (M > 0 && N == 0) {
        for (size_t i = M - 1; i < M + 2; i++) {
          for (size_t j = N; j < N + 2; j++) {
            int actual[2] = {M, N};
            int posible[2] = {i, j};
            if (menor(actual, posible)) {
              exlplorarNodo(i, j);
            }
          }
        }
      } else if (M == 0 && N == 0) {
        for (size_t i = M; i < M + 2; i++) {
          for (size_t j = N; j < N + 2; j++) {
            int actual[2] = {M, N};
            int posible[2] = {i, j};
            if (menor(actual, posible)) {
              exlplorarNodo(i, j);
            }
          }
        }
      }
    }
  }
}

int menor(int M[], int N[]) {
  if (sqrt((pow((B[0] - M[0]), 2) + pow((B[1] - M[1]), 2))) >
      sqrt((pow((B[0] - N[0]), 2) + pow((B[1] - N[1]), 2)))) {
    return 1;
  } else {
    return 0;
  }
}
