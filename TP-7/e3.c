// Listas enlazadas
// Implemente una biblioteca con funciones para manipulación de listas
// doblemente enlazadas. Cree un nodo para la lista mediante struct (el
// contenido de cada nodo es a libre elección del alumno), e implemente
// funciones para
// 5. agregar un elemento a la lista,
// 6. eliminar un elemento de la lista,
// 7. modificar un elemento de la lista
// 8. mostrar todos los elementos de la lista.

#include <stdio.h>
#include <stdlib.h>

struct Nodo {
  int data;
  struct Nodo *next;
  struct Nodo *prev;
};

struct Nodo *nuevoNodo(int data);
void insert(int dato, int index);
void printNodos();
void append(int dato);
void printNodosReverso();
void removeNodo(int index);

struct Nodo *inicio;
struct Nodo *final;

int main(int argc, char const *argv[]) {
  inicio = NULL;
  final = NULL;
  printf("Creación de la lista\n");
  for (size_t i = 0; i < 15; i++) {
    append(27);
    // printNodos();
  }
  printf("lista Inicial\n");
  printNodos();
  printf("Insertamos elemento nuevo en la posición 3 de la lista\n");
  insert(98, 2);
  printNodos();
  printf("Lo hacemos 4 veces más\n");
  for (size_t i = 0; i < 4; i++) {
    insert(98, i + 2);
  }
  printNodos();
  printf("Leemos la lista en reverso en una demostración de poder\n");
  printNodosReverso();
  printf("Eliminamos 4 elementos de la lista\n");
  for (size_t i = 0; i < 4; i++) {
    removeNodo(4);
  }
  printNodos();

  return 0;
}

struct Nodo *nuevoNodo(int dato) {
  struct Nodo *nuevoNodo = (struct Nodo *)malloc(sizeof(struct Nodo));
  nuevoNodo->data = dato;
  nuevoNodo->next = NULL;
  nuevoNodo->prev = NULL;
  return nuevoNodo;
}

void insert(int dato, int index) {
  struct Nodo *temp = inicio;
  struct Nodo *NNodo = nuevoNodo(dato);
  if (inicio == NULL) {
    inicio = NNodo;
    return;
  }
  for (size_t i = 0; i < index; i++) {
    if (temp->next != NULL) {
      temp = temp->next;
    } else {
      // append(dato);
      return;
    }
  }
  NNodo->next = temp->next;
  temp->next->prev = NNodo;
  temp->next = NNodo;
  NNodo->prev = temp;
}

void printNodos() {
  struct Nodo *temp = inicio;
  printf("Nodos\t");
  while (temp->next != NULL) {
    temp = temp->next;
    printf("->%d", temp->data);
  }
  printf("%s\n");
}

void printNodosReverso() {
  // struct Nodo *temp = final;
  struct Nodo *temp = inicio;
  while (temp->next != NULL) {
    temp = temp->next;
  }
  printf("Nodos\t");
  while (temp->prev != NULL) {
    printf("<-%d", temp->data);
    temp = temp->prev;
  }
  printf("\n");
}

void append(int dato) {
  struct Nodo *temp = inicio;
  struct Nodo *NNodo = nuevoNodo(dato);
  if (inicio == NULL) {
    inicio = NNodo;
    return;
  }
  while (temp->next != NULL) {
    temp = temp->next;
  }
  temp->next = NNodo;
  NNodo->prev = temp;
  // NNodo->next = final;
  // final->prev = NNodo;
}

void removeNodo(int index) {
  struct Nodo *temp = inicio;
  for (size_t i = 0; i < index; i++) {
    if (temp->next != NULL) {
      temp = temp->next;
    } else {
      return;
    }
  }
  struct Nodo *temp2 = temp->next->next;
  free(temp->next);
  temp->next = temp2;
}
