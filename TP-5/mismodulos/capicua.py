from heapq import nsmallest


def capicuas(a):
    if len(a) > 6:
        print("Demasiadas cifras.")
        return 0
    if a[::] == a[::-1]:
        b = int(a) + 1
    else:
        b = int(a)
    capis = []
    resto = []
# hace falta optimizar aqui
    for j in range(b, 0, -1):
        i = str(j)
        if i[::] == i[::-1] and len(capis) < 3:
            capis.append(j)
    capis.reverse()
# hace falta optimizar aqui
    for j in range(b, 999999):
        i = str(j)
        if i[::] == i[::-1] and len(capis) < 6:
            capis.append(j)
    resto = nsmallest(3, capis, key=lambda x: abs(x-b))
    return resto
