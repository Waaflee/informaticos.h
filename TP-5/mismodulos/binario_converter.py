def toDecimal(a):
    decimal = 0
    k = 0
    for i in a:
        k += 1
        decimal += int(i) * 2**(len(a) - k)
    # print(decimal)
    return decimal


def toHexadecimal(a):
    valores = {10: 'A', 11: 'B', 12: 'C', 13: 'D', 14: 'E', 15: 'F'}
    b = 4 - (len(a) % 4)
    a = b*"0" + a
    hexa = ""
    b = [a[i:i+4] for i in range(0, len(a), 4)]
    for i in b:
        k = toDecimal(i)
        if k > 9:
            a = valores[k]
        else:
            a = str(k)
        hexa += a
    for i in hexa:
        if hexa[0] == "0":
            hexa = hexa[1:]
    # print(hexa)
    return hexa


def toOctal(a):
    b = 3 - (len(a) % 3)
    a = b*"0" + a
    octal = ""
    b = [a[i:i+3] for i in range(0, len(a), 3)]
    for i in b:
        k = toDecimal(i)
        octal += str(k)
    # print(octal)
    for i in octal:
        if octal[0] == "0":
            octal = octal[1:]
    return octal
