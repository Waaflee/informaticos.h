// Escriba un programa que:
// a. lea por teclado un entero N,
// b. cree 4 vectores de N elementos double dinámicamente con valores aleatorios,
// c. lea por teclado un factor de escala double
// d. escale 2 de los vectores generados en (b) por el factor s
// e. calcule el producto vectorial de los vectores de a pares, el primero por el segundo y
// el tercero por el cuarto, generando como resultado 2 vectores u y v
// f. calcule el producto vevtorial de u x v
// g. muestre todos los resultados intermedios por pantalla
// h. Libere la memoria dinámica utilizada
#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  set_seed();
  int N;
  // printf("Ingrese longitud de los vectores: ");
  // scanf("%d", &N);
  int valoracceptable = 0;
  while (!valoracceptable) {
    printf("Ingrese longitudes de los vectores: ");
    scanf("%d", &N);
    if (N < 4 && N > 0) {
      valoracceptable = 1;
    } else {
      printf("Por favor ingrese un valor entre 1 y 3.\n");
    }
  }

  double *a = crear_vector(N);
  cargar_vector(a, N);
  printf("vector 'a': ");
  mostrar_vector(a, N);

  double *b = crear_vector(N);
  cargar_vector(b, N);
  printf("vector 'b': ");
  mostrar_vector(b, N);

  double *c = crear_vector(N);
  cargar_vector(c, N);
  printf("vector 'c': ");
  mostrar_vector(c, N);

  double *d = crear_vector(N);
  cargar_vector(d, N);
  printf("vector 'd': ");
  mostrar_vector(d, N);

  double s;
  printf("Ingrese escalar 's': ");
  scanf("%le", &s);

  producto_escalar(a, s, N);
  printf("vector 'a' escalado por %f: ", s);
  mostrar_vector(a, N);

  producto_escalar(b, s, N);
  printf("vector 'b' escalado por %f: ", s);
  mostrar_vector(b, N);

  double *u = producto_vectorial(a, b, N);
  printf("vector 'u': ");
  mostrar_vector(u, 3);

  double *v = producto_vectorial(c, d, N);
  printf("vector 'v': ");
  mostrar_vector(v, 3);

  double *w = producto_vectorial(u, v, N);
  printf("Producto vectorial entre u y v: ");
  mostrar_vector(w, 3);

  borrar_vector(a);
  borrar_vector(b);
  borrar_vector(c);
  borrar_vector(d);
  borrar_vector(u);
  borrar_vector(v);
  borrar_vector(w);

  return 0;
}
