// Repita el ejercicio (1) para el caso de una matriz de M x N elementos.
//  -> Escriba una función que reciba como argumento un entero N, cree un vector de N
// elementos de tipo double dinámicamente (utilizando la función malloc), y devuelva un
// apuntador con la dirección de memoria del arreglo creado

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  set_seed();
  int M, N;
  printf("Ingrese la cantidad de filas deseadas: ");
  scanf("%d", &M);
  printf("Ingrese la cantidad de columnas deseadas: ");
  scanf("%d", &N);
  double **w = crear_matriz(M, N);
  mostrar_matriz(w, M, N);
  // cargar_matriz(w, M, N);
  // mostrar_matriz(w, M, N);
  return 0;
}
