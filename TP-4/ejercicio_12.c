// Escriba una función que reciba como parámetro una matriz de M x N elementos, y calcule
// la transpuesta de la matriz. El resultado debe ser almacenado en la misma matriz (es decir
// que no se debe reservar memoria para el resultado ni devolver ningún apuntador)

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {

  int M, N;
  printf("ingrese dimensiones MxN de la matriz A (ej.'5x4'): ");
  scanf("%dx%d", &M, &N);
  double **A = crear_matriz(M, N);
  cargar_matriz(A, M, N);
  printf("Matriz A: \n");
  mostrar_matriz(A, M, N);
  transponer_matriz(&A, M, N);
  mostrar_matriz(A, N, M);
  borrar_matriz(A, M);

  return 0;
}
