// Escriba una función que reciba como argumento un vector de N elementos double (donde
// N puede ser variable) y que genere valores aleatorios para el mismo. Asegurese de
// generar valores con cifras decimales (es decir que no sean simplemente enteros).
#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  set_seed();
  int N;
  printf("Ingrese longitud del vector: ");
  scanf("%d", &N);
  double *u = crear_vector(N);
  cargar_vector(u, N);
  printf("Vector: ");
  mostrar_vector(u, N);

  return 0;
}
