// Escriba una función que reciba como parámetros 2 matrices A y B de MxN y NxP
// elementos tipo double, respectivamente, cree una nueva matriz C de MxP elementos
// dinámicamente, y calcule (almacenando el resultado en C) el producto matricial. La función
// debe devover el apuntador a C (que fue creada en la misma función). Valide los tamaños
// de las matrices
#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  set_seed();
  int M, N, n, P, ok = 0;
  while (ok == 0) {
    printf("ingrese dimensiones MxN de la matriz A (ej.'5x4'): ");
    scanf("%dx%d", &M, &N);
    printf("ingrese dimensiones NxP de la matriz B (ej.'4x6'): ");
    scanf("%dx%d", &n, &P);
    if (n == N) {
      ok = 1;
    } else if (n != N) {
      printf("-> %d es distinto de %d, deben ser iguales.\n", n, N);
    }
  }
  double **A = crear_matriz(M, N);
  cargar_matriz(A, M, N);
  printf("Matriz %c:\n\n", 'A');
  mostrar_matriz(A, M, N);
  double **B = crear_matriz(N, P);
  cargar_matriz(B, N, P);
  printf("Matriz %c:\n\n", 'B');
  mostrar_matriz(B, N, P);
  double **C = producto_matricial(A, B, M, N, P);
  printf("Producto de las matrices A(%d,%d)*B(%d,%d):\n",M,N,N,P);
  mostrar_matriz(C, M, P);

  return 0;
}
