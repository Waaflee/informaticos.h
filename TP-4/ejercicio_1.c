// Escriba una función que reciba como argumento un entero N, cree un vector de N
// elementos de tipo double dinámicamente (utilizando la función malloc), y devuelva un
// apuntador con la dirección de memoria del arreglo creado
#include "../lib/allocate.h"


int main(int argc, char const *argv[]) {
  int N;
  printf("Ingrese longitud del vector: ");
  scanf("%d", &N);
  double *vector = crear_vector(N);
  printf("El puntero %f ubica %p en %p\n", *vector, vector, &vector);
}
