// Escriba un programa que:
// a. Lea por teclado 3 valores M, N y P
// b. Cree 4 matrices dinámicamente con valores aleatorios:
// i. 2 de M x N y
// ii. 2 de N x P
// c. Lea por teclado un factor de escala double
// d. Escale las 4 matrices generadas en (b) por el factor s
// e. Calcule el producto matricial de a pares, generando 2 matrices C 1 y C 2 de MxP
// elementos cada una
// f. Calcule la transpuesta de C 2
// g. Calcule el producto de C 1 x C 2T (MxP y PxM respectivamente)
// h. Muestre los resultados intermedios por pantalla
// i. Libere la memoria dinámica utilizada

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {

  int M, N, P;
  printf("ingrese dimensiones de las matrices MxN y NxP:\n");
  printf("Infrese dimensión M: ");
  scanf("%d", &M);
  printf("Infrese dimensión N: ");
  scanf("%d", &N);
  printf("Infrese dimensión P: ");
  scanf("%d", &P);
  double **A = crear_matriz(M, N);
  cargar_matriz(A, M, N);
  double **B = crear_matriz(M, N);
  cargar_matriz(B, M, N);
  double **C = crear_matriz(N, P);
  cargar_matriz(C, M, N);
  double **D = crear_matriz(N, P);
  cargar_matriz(D, M, N);

  double s;
  printf("Ingrese escalar 's': ");
  scanf("%le", &s);

  escalar_matriz(A, M, N, s);
  printf("Matriz A escalada por %f:\n", s);
  mostrar_matriz(A, M, N);
  escalar_matriz(B, M, N, s);
  printf("Matriz B escalada por %f:\n", s);
  mostrar_matriz(B, M, N);
  escalar_matriz(C, N, P, s);
  printf("Matriz C escalada por %f:\n", s);
  mostrar_matriz(C, M, N);
  escalar_matriz(D, N, P, s);
  printf("Matriz D escalada por %f:\n", s);
  mostrar_matriz(D, M, N);

  double **C1 = producto_matricial(A, C, M, N, P);
  printf("Matriz C1 producto entre A y C:\n");
  mostrar_matriz(C1, M, P);
  double **C2 = producto_matricial(B, D, M, N, P);
  printf("Matriz C2 producto entre B y D:\n");
  mostrar_matriz(C2, M, P);
  borrar_matriz(A, N);
  borrar_matriz(B, N);
  borrar_matriz(C, P);
  borrar_matriz(D, P);

  transponer_matriz(&C2, M, P);
  mostrar_matriz(C2, P, M);
  double **C3 = producto_matricial(C1, C2, M, P, M);
  printf("Matriz C3 producto entre C1 y C2T:\n");
  mostrar_matriz(C3, M, M);
  borrar_matriz(C1, P);
  borrar_matriz(C2, M);

  return 0;
}
