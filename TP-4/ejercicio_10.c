// Repita el ejercicio (4) para el caso de una matriz de M x N elementos
// -> Escriba una función que reciba como argumento un vector de N elementos double (donde
// N puede ser variable) y que genere valores aleatorios para el mismo. Asegurese de
// generar valores con cifras decimales (es decir que no sean simplemente enteros).

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  
  int M, N;
  printf("ingrese dimensiones MxN de la matriz A (ej.'5x4'): ");
  scanf("%dx%d", &M, &N);
  double **A = crear_matriz(M, N);
  cargar_matriz(A, M, N);
  printf("Matriz A: \n");
  mostrar_matriz(A, M, N);

  return 0;
}
