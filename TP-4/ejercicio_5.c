// Escriba una función que reciba un vector de N elementos (N variable) e imprima por
// pantalla el vector en un formato legible para el ser humano (ej: utilizando notación
// algebraica: “ (v1, v2, …, vn) ”)

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  set_seed();
  int N;
  printf("Ingrese longitud del vector: ");
  scanf("%d", &N);
  double *u = crear_vector(N);
  cargar_vector(u, N);
  printf("Vector: ");
  mostrar_vector(u, N);

  return 0;
}
