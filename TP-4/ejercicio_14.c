// La Sucesión de Fibonacci viene dada por las siguientes ecuaciones:
// Fn=Fn−1+Fn−2
// F0=1
// F1=1
// Escriba una función recursiva que reciba como argumento un entero n, y que calcule la
// sucesión hasta el valor Fn
#include <stdio.h>

long int Fibonacci(int n) {
  if (n == 0 || n == 1){
    return 1;
  } else if (n > 1) {
    return (Fibonacci(n - 1) + Fibonacci(n - 2));
  }
}

void calc_fibonacci(int n){
  printf("0 ");
  for (size_t i = 0; i < n; i++) {
    printf("%ld ", Fibonacci(i));
  }
}

int main(int argc, char const *argv[]) {
  int n;
  printf("Ingrese un argumento entero: ");
  scanf("%d", &n);
  calc_fibonacci(n);
  return 0;
}
