// Escriba una función que reciba como parámetros 2 vectores u y v de N elementos tipo
// double (donde N puede ser variable), cree un nuevo vector w de N elementos
// dinámicamente, y calcule (almacenando el resultado en w) el producto vectorial u x v. La
// función debe devover el apuntador a w (que fue creado en la misma función).
#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  set_seed();
  int N;
  int valoracceptable = 0;

  while (!valoracceptable) {
    printf("Ingrese longitudes de los vectores 'u' y 'v': ");
    scanf("%d", &N);
    if (N < 4 && N > 0) {
      valoracceptable = 1;
    } else {
      printf("Por favor ingrese un valor entre 1 y 3.\n");
    }
  }

  double *u = crear_vector(N);
  cargar_vector(u, N);
  double *v = crear_vector(N);
  cargar_vector(v, N);
  
  double *w = producto_vectorial(u, v, N);

  printf("w es igual a (%f,%f,%f)\n", w[0], w[1], w[2]);

  return 0;
}
