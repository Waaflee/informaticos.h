// Repita el ejercicio (5) para el caso de una matriz de M x N elementos
// -> Escriba una función que reciba un vector de N elementos (N variable) e imprima por
// pantalla el vector en un formato legible para el ser humano (ej: utilizando notación
// algebraica: “ (v1, v2, …, vn) ”)

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {

  int M, N;
  printf("ingrese dimensiones MxN de la matriz A (ej.'5x4'): ");
  scanf("%dx%d", &M, &N);
  double **A = crear_matriz(M, N);
  cargar_matriz(A, M, N);
  printf("Matriz A: \n");
  mostrar_matriz(A, M, N);

  return 0;
}
