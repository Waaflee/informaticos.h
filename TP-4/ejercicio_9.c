// Repita el ejercicio (3) para el caso de una matriz de M x N elementos
// -> Escriba una función que reciba un vector de N elementos double (donde N pued ser
// variable) y un escalar double s, y escale el mismo arreglo utilizando el factor s (es decir, no
// debe devolver un puntero ni crear otro arreglo)

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"

int main(int argc, char const *argv[]) {
  int M, N;
  double s;
  printf("ingrese dimensiones MxN de la matriz A (ej.'5x4'): ");
  scanf("%dx%d", &M, &N);
  printf("ingrese escalar 's': ");
  scanf("%le", &s);
  double **A = crear_matriz(M, N);
  cargar_matriz(A, M, N);
  printf("Matriz A: \n");
  mostrar_matriz(A, M, N);
  escalar_matriz(A, M, N, s);
  printf("Matriz A escalada por %f: \n", s);
  mostrar_matriz(A, M, N);

  return 0;
}
