// Escriba una función que reciba un vector de N elementos double (donde N pued ser
// variable) y un escalar double s, y escale el mismo arreglo utilizando el factor s (es decir, no
// debe devolver un puntero ni crear otro arreglo)

#include "../lib/allocate.h"
#include "../lib/vectorial_prod.h"


int main(int argc, char const *argv[]) {
  set_seed();
  int N;
  printf("Ingrese longitud del vector: ");
  scanf("%d", &N);
  double *u = crear_vector(N);
  double s;
  printf("Ingrese escalar 's': ");
  scanf("%le", &s);
  cargar_vector(u, N);
  printf("Vector: ");
  mostrar_vector(u, N);
  producto_escalar(u, s, N);
  printf("Vector resultado: ");
  mostrar_vector(u, N);

  return 0;
}
