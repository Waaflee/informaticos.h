// Un sistema en un parque eólico predice la producción de energía del día siguiente Dt+1. La
// predicción se realiza promediando la producción de energía en el día Dt, y luego
// promediando esta producción promedio en Dt con la producción promedio de los 20 días
// anteriores. El precio de venta del MW es de U$D 100. Utilice una estructura de datos
#include <stdio.h>
#include "../../lib/caos.h"
MAXIMO = 250;

//gcc -o eje16 ejercicio_16.c ../../lib/caos.c

int main(int argc, char const *argv[]) {

  time_t segundosEpoch = time(NULL);
  srand(segundosEpoch);
  int sum = 0;
  int produccion[3][7]; // cada fila es una semana y cada columna un dia;

  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 7; j++) {
      if (i == 3 && j == 7) {
        produccion[i][j] = 0.00;
      } else {
        produccion[i][j] = caos();
      }
    }
  }

  int promedio = 0;      //se ha guardado el promedio de produccion de cada dia;
  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 7; j++) {
      if (i == 1 && j == 6) {
        continue;
      } else if (i == 2 && j == 6) {
        continue;
      } else {
        sum += produccion[i][j];
      }
    }
  }

 promedio = sum / 19;
 produccion[2][6] = (promedio + produccion[2][5]) / 2;
 printf("Hoy se produjeron %dMW, se esperan para mañana unos %dMW\n", produccion[2][5], produccion[2][6]);

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 7; j++){
      printf("%dMW\t", produccion[i][j]);
    }
    printf("\n");
  }


  return 0;
}
