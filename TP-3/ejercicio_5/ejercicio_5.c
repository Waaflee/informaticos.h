// Escribir un programa que resuelva los valores resultantes a las siguientes funciones
// multivariables. Considere las variables como valores enteros que se le piden al usuario:
#include <stdio.h>
#include <math.h>

float funcionX() {
  float a, b, c, d, e;
  printf("X(a,b,c,d,e) -> ingrese a, b, c, d, e\n");
  scanf("%f%f%f%f%f", &a, &b, &c, &d, &e);
  float resultado = ((3 * a) + b)/(c - ((d + 5 * e)/(a - b)));
  return resultado;
}
float funcionY() {
  float a, b, c;
  printf("Y(a,b,c) -> ingrese a, b, c\n");
  scanf("%f%f%f", &a, &b, &c);
  float resultado = 3 * pow( a, 4) - 5*pow( b, 3) + pow( c, 12) -7;
  return resultado;
}

int main(int argc, char const *argv[]) {
  float X = funcionX();
  float Y = funcionY();

  printf("X=%f\n", X);
  printf("Y=%f\n", Y);  

  return 0;
}
