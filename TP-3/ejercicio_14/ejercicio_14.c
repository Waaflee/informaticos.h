// Escriba un programa que calcule el producto de 2 matrices A x B = C, con Amn, Bnp.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAXIMO 10



int azar(void) {
  return rand() % MAXIMO;
}

int main(int argc, char const *argv[]) {
  time_t segundosEpoch = time(NULL);
  srand(segundosEpoch);
  int m = azar(), n = azar() ,p = azar(), sum = 0;
  int A[m][n];
  for (size_t i = 0; i < m; i++) {
      for (size_t j = 0; j < n; j++) {
        A[i][j] = azar();
        printf("%d\n", A[i][j]);
      }
  }
  int B[n][p];
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < p; j++) {
          B[i][j] = azar();
          printf("%d\n", B[i][j]);
        }
    }

  int C[m][p];
  for (size_t i = 0; i < m; i++) {
      for (size_t j = 0; j < p; j++) {
        C[i][j] = 0;
      }
  }

  for (size_t i = 0; i < m; i++) {
    for (size_t j = 0; j < p; j++) {
      for (size_t k = 0; k < n; k++) {
        sum += A[i][k] * B[k][j];

      }

      C[i][j] = sum;
      sum = 0;
    }
  }

  printf("Producto de las matrices A(%d,%d)*B(%d,%d):\n",m,n,n,p);

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < p; j++){
        printf("%d\t", C[i][j]);
      }

      printf("\n");
    }


  return 0;
}
