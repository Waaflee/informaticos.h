// Escribir un programa que pida 3 números por pantalla e identifique cual es el valor central,
// si es posible. Ej. a es central si y solo si b > a > c.
#include <stdio.h>

int main(int argc, char const *argv[]) {
  printf("Ingrese 3 números naturales\n");
  int a,b,c;
  scanf("%d%d%d", &a, &b, &c);
  // printf("a=%d\nb=%d\nc=%d\n", a, b, c);
  if ((b > a && a > c) || (c > a && a > b)) {
    printf("%d es el valor central\n", a);
  } else if ((a > b && b > c) || (c > b && b > a)) {
    printf("%d es el valor central\n", b);
  } else if ((a > c && c > b) || (b > c && c > a)) {
    printf("%d es el valor central\n", c);
  } else {
    printf("Algo no está bien.\n");
  }
  return 0;
}
