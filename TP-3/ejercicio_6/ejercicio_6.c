// Escribir un programa que le pida al usuario una fecha del estilo DD/MM/AAAA y determine:
// a. El día anterior y posterior.
// b. El último día del mes y cuantos días faltan para el mismo.
// Considere la existencia de los años bisiestos.
#include <stdio.h>

int ultimoDia(int MM, int AAAA) {
  int maxDias;
  switch (MM) {
    case 1:maxDias = 31;
    break;
    case 2:
          if (AAAA%4 == 0) {
            maxDias = 29;
          }else{
            maxDias = 28;
          }
    break;
    case 3:maxDias = 31;
    break;
    case 4:maxDias = 30;
    break;
    case 5:maxDias = 31;
    break;
    case 6:maxDias = 30;
    break;
    case 7:maxDias = 31;
    break;
    case 8:maxDias = 31;
    break;
    case 9:maxDias = 30;
    break;
    case 10:maxDias = 31;
    break;
    case 11:maxDias = 30;
    break;
    case 12:maxDias = 31;
    break;
    default: maxDias = 30;
  }
  return maxDias;
}

void gimmeManiana(int dia, int mes, int anio) {

  int tomorrow, mesTomorrow, yearTomorrow; //no acepta ñ, anglosajones irrespetuosos.
  if (dia == ultimoDia(mes, anio)) {
    tomorrow = 1;
    mesTomorrow = mes + 1;
    if (mes == 12){
      mesTomorrow = 1;
      yearTomorrow = anio + 1;
    }
  }else{
    tomorrow = dia + 1;
    mesTomorrow = mes;
    yearTomorrow = anio;
  }
  printf("Mañana será %d/%d/%d\n", tomorrow, mesTomorrow, yearTomorrow);
}

void gimmeAyer(int dia, int mes, int anio){
  int ayer, mesAyer, yearAyer;
  if (dia > 1) {
    ayer = dia - 1;
    mesAyer = mes;
    yearAyer = anio;
  } else if (mes == 1) {
    ayer = ultimoDia( 12, anio - 1);
    mesAyer = 12;
    yearAyer = anio - 1;
  } else {
    ayer = ultimoDia(mes - 1, anio);
    mesAyer = mes;
    yearAyer = anio;
  }
  printf("Ayer fué %d/%d/%d\n", ayer, mesAyer, yearAyer);
}

int main(int argc, char const *argv[]) {


  printf("ingrese fecha DD/MM/AAAA\n");
  int DD, MM, AAAA;

  printf(" ->  ");
  scanf("%2d/%2d/%4d", &DD, &MM, &AAAA);

  int diamax = ultimoDia(MM, AAAA);

  if (DD > diamax || DD < 1 || MM < 1 || MM > 12 || (DD == 29 && MM == 2 && AAAA % 4 != 0)) {
    printf("¡Fecha no valida!\n");

  } else {

    // printf("DD:");
    // scanf("%2d", &DD);
    // printf("MM:");
    // scanf("%2d", &MM);
    // printf("AAAA:");
    // scanf("%4d", &AAAA);

    int restante = ultimoDia(MM, AAAA) - DD;

    gimmeAyer(DD, MM, AAAA);
    gimmeManiana(DD, MM, AAAA);
    printf("El Último día de este mes es el %d, y para ello faltan %d días.\n", ultimoDia(MM, AAAA),restante);

  }


  return 0;
}
