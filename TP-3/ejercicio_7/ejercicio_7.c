// Escribir un programa que calcule el factorial de un número y muestre el valor por pantalla
#include <stdio.h>
#include <stdlib.h>

int factorial(int);

int main() {
  int num;
  printf("Ingrese un entero:");
  scanf("%d", &num);
  int facto = factorial(num);
  printf("El factorial de %d es %d\n", num, facto);
  return 0;
}

int factorial(int numero) {
  if (numero == 0)
    return 1;
  else
    return(numero * factorial(numero - 1));
}
