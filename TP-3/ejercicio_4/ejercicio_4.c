// Escribir un programa que lea 2 números enteros por teclado y que calcule:
// a. el número que contiene sólo los bits que son 1 en ambos números
// b. el número que contiene los bits que son 1 en alguno de los números

#include <stdio.h>

int main(int argc, char const *argv[]) {
  int a,b;
  printf("ingrese a y b (números enteros)\n");
  scanf("%d%d", &a, &b);
  int A = a & b;
  int B = a | b;
  printf("el número que contiene sólo los bits que son 1 en ambos números:%d\n", A);
  printf("el número que contiene los bits que son 1 en alguno de los números:%d\n", B);
  return 0;
}
