#include <stdio.h>

int main(int argc, char const *argv[]) {
  float sum = 0;
  for (int i = 0; i <= 100; i++) {
    for (int j = 0; j <= 100; j++) {
      if ((i % 2 == 0) && (j % 2 == 0) && (i != 0 || j != 0)) {
        sum += ((float)(i * j)) / ((float)(i + j));
      }
    }
  }
  printf("El resultado es: %f\n", sum);
  return 0;
}
