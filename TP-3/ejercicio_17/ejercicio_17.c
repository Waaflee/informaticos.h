// Escriba un programa que calcule el producto vectorial de 2 vectores en R 3 utilizando structs
// pare representar los vectores
#include <stdio.h>
#include "../../lib/caos.h"
//gcc -o eje17 ejercicio_17.c ../../lib/caos.c

#define MAXIMO 15

typedef struct vector {
  int x;
  int y;
  int z;
} Vector;

int main(int argc, char const *argv[]) {

  time_t segundosEpoch = time(NULL);
  srand(segundosEpoch);

  Vector A = {
   caos(MAXIMO),
   caos(MAXIMO),
   caos(MAXIMO)
};

  Vector B;
  B.x = caos(MAXIMO);
  B.y = caos(MAXIMO);
  B.z = caos(MAXIMO);

  Vector C;
  C.x = A.y * B.z - (B.y * A.z);
  C.y = - (A.x * B.z - B.x * A.z);
  C.z = A.x * B.y - (B.x * A.y);

  printf(" [%d,%d,%d]x[%d,%d,%d] = [%d,%d,%d] \n", A.x, A.y,A.z, B.x, B.y, B.z, C.x, C.y, C.z);


  return 0;
}
