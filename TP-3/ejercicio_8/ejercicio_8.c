// Escribir un programa que permita ingresar por teclado un valor inicial a, un valor final b, y
// un número c, y que cuente la cantidad de números divisibles por c que hay en el rango
// [a,b].
#include <stdio.h>

int main() {

int a, b, c;
printf("Ingrese rango a -> b:");
scanf("%d%d", &a, &b);
printf("Ingrese argumento c:");
scanf("%d", &c);

int divisibles = 0;
for (size_t i = a; i < b; i++) {
      if (i%c == 0){
        divisibles++;
      }
}
printf("Hay %d numeros divisibles por %d en el intervalo [%d,%d]", divisibles, c, a, b);
}
