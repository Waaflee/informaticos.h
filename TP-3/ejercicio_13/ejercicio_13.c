// Escriba un programa que
// a. Genere un vector con valore aleatorios enteros
// b. Recorra el vector y guarde el promedio de los valores en una variable
// c. Encuentre el menor de los elementos

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define MAXIMO 500000

int main(int argc, char const *argv[]) {
  time_t segundosEpoch = time(NULL);
  srand(segundosEpoch);
  int azar = rand() % MAXIMO, maximo, minimo, maxIndex, minIndex, sum;
  printf("Un vector de %d componentes.\n", azar);
  int vector[azar];
  maximo = 0;
  minimo = MAXIMO;
  sum = 0;

  for (size_t i = 0; i < azar; i++) {
    vector[i] = rand() % MAXIMO;
    if (vector[i] > maximo) {
      maximo = vector[i];
      maxIndex = i + 1;
    }
    if (vector[i] < minimo) {
      minimo = vector[i];
      minIndex = i + 1;
    }
    sum += vector[i];
  }
  float promedio = sum / azar;
  printf("Tiene un valor máximo = %d\nUn valor mínimo = %d\nY un valor promedio = %f\n", maximo, minimo, promedio);
  return 0;
}
