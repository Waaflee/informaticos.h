// La compañía de celulares “Chismefon” posee un mecanismo de cobro de llamadas por el
// cual mientras más se habla, menos se paga. De esta forma los primeros cinco minutos
// cuestan $ 1.00 c/u, los siguientes tres, $ 0.80 c/u, los siguientes dos minutos, $ 0.70 c/u, y a
// partir del décimo minuto, 0.50 c/u (los valores no incluyen IVA). Realice un programa para
// determinar el costo total de una llamada expresada en segundos.

#include <stdio.h>
#include <stdlib.h>

float laCuentaPorfavor(int minutos){
  minutos = minutos / 60;
  float total = 0.00;
  if (minutos < 1) {
       total = 1.00;
  } else if (minutos < 6 && minutos > 1) {
       total = minutos * 1.00;
  } else if (minutos < 9 && minutos > 5) {
      total = 5.00 + (minutos - 5)* 0.80;
  } else if (minutos < 11 && minutos > 8){
      total = 7.40 + (minutos - 8) * 0.70;
  } else if (minutos > 10) {
      total = 8.80 + (minutos - 10) * 0.50;
  }
  return total;
}

int main(int argc, char const *argv[]) {

  if (argc > 1) {

    int segs = atoi(argv[1]);
    float money = laCuentaPorfavor(segs);
    printf("%ds el total seria: %f, muchas gracias por utilizar Chismefon.\n", segs ,money);

  } else {
    printf("Debes indicar el nº de segundos hablados como un argumento de entrada\n");
    printf("modo de uso: %s <número de segundos>\n", argv[0]);
  }

  return 0;
}
