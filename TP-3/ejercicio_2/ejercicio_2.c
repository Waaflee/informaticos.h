#include <stdio.h>
#include <ctype.h>

int main(int argc, char const *argv[]) {
  int count;
  // char letra = argv[1];
  if (argc > 1) {
      for (count = 1; count < argc; count++) {

          switch (tolower(*argv[count])) {
            case 'a':printf("Vocal!\n");
            break;
            case 'e':printf("Vocal!\n");
            break;
            case 'i':printf("Vocal!\n");
            break;
            case 'o':printf("Vocal!\n");
            break;
            case 'u':printf("Vocal!\n");
            break;
            default:printf("No es vocal :(\n");
          }
      }
  } else {
    printf("El uso es: %s <caracter>\n", argv[0]);
  }
  return 0;
}
